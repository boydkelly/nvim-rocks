vim.opt.keymap = "dyu"
vim.opt.spell = true
-- :imap <expr> " getline('.')[col('.')-2]=~'\S' ?  ' »' : '« '
-- ":imap <expr> ' getline('.')[col('.')-2]=~'\S' ?  ' »' : '« '
if pcall(require, "nvim-autopairs") then
require("nvim-autopairs").remove_rule('"') -- remove rule "
end
require("utils").toggleGuillemets()
vim.opt.background = "dark"
