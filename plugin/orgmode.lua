local opts = {
  org_agenda_files = { "~/gtd/pages/*" },
  org_default_notes_file = "~/gtd/pages/Contents.org",
  -- org_agenda_files = { vim.env.HOME .. "/org-notes/agenda/*" },
  -- org_default_notes_file = vim.env.HOME .. "/org-notes/default.org",
}

local module_name = "orgmode"
local status_ok, module = pcall(require, module_name)
if not status_ok then
  vim.notify("Couldn't load module '" .. module_name .. "'")
  do return end
end

-- require("nvim-treesitter.configs").setup {
--   -- If TS highlights are not enabled at all, or disabled via `disable` prop, highlighting will fallback to default Vim syntax highlighting
--   org_todo_keywords = { "NOW", "LATER", "|", "DONE" },
--   highlight = {
--     enable = true,
--     additional_vim_regex_highlighting = { "org" }, -- Required since TS highlighter doesn't support all syntax features (conceal)
--   },
--   ignore_install = { "org" }
-- }

-- return module.setup(opts)
-- local safe_require = require("safe_require")
--
-- safe_require("orgmode").setup(opts)
