function! Ventilated()
"NOTE: works with gq but not gw this is what we want
  setl formatexpr=OneSentencePerLine()
endfunction

function! OneSentencePerLine()
  if mode() =~# '^[iR]'
    return
  endif
  let start = v:lnum
  let end = start + v:count - 1
  execute start.','.end.'join'
  s/[.:;?!'"”»][ ]\zs\s*\ze\S/\r/g
endfunction
"set formatexpr=OneSentencePerLine()
