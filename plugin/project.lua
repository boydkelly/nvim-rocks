--   "ahmedkhalf/project.nvim",
require("project_nvim").setup {
  detection_methods = { "pattern", "lsp" },
  patterns = {
    "pubs",
    ".git",
    "package.json",
    "go.mod",
    "=content",
    "=translate",
    "=search",
    -- "=.config",
    -- "=.config/nvim-*",
  },
  ignore_lsp = { "null-ls" },
  exclude_dirs = { ".config/nvim-*" },
  silent_chdir = false 
}
