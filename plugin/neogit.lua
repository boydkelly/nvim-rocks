require("neogit").setup {
      integrations = { diffview = true },
      disable_commit_confirmation = true,
}
