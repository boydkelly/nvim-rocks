-- "stevearc/conform.nvim",
vim.keymap.set("n", "<leader>cf",
  function() require("conform").format { async = true, lsp_fallback = true }end, { desc = "Format buffer" })
local opts = {
  -- Define your formatters
  formatters_by_ft = {
    sh = { "shfmt" },
    lua = { "stylua" },
    python = { "isort", "black" },
    javascript = { { "prettierd", "prettier" } },
    typescript = { "prettier" },
    yaml = { "yamlfmt" },
    -- yaml = { "yamlfix" },
    svelte = { "prettier" },
    css = { "prettier" },
    html = { "prettier" },
    json = { "prettier" },
    markdown = { "prettier" },
    graphql = { "prettier" },
  },
  -- Set up format-on-save
  -- format_on_save = { timeout_ms = 10000, lsp_fallback = true },
  -- Customize formatters
  formatters = {
    yamlfix = {
      -- Change where to find the command
      -- command = "local/path/yamlfix",
      -- Adds environment args to the yamlfix formatter
      env = {
        YAMLFIX_SEQUENCE_STYLE = "block_style",
        YAMLFIX_LINE_LENGTH = "150",
      },
    },
    shfmt = {
      prepend_args = { "-i", "2" },
    },
  },
}
  -- If you want the formatexpr, here is the place to set it
  -- vim.o.formatexpr = "v:lua.require'conform'.formatexpr()"
require("conform").setup(opts)
