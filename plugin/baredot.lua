local opts = {
  git_dir = "~/.cfg"
}

local module_name = "baredot"
local status_ok, module = pcall(require, module_name)
if not status_ok then
  vim.notify("Couldn't load module '" .. module_name .. "'")
  do return end
end
-- print(module_name, module)
return module.setup(opts)
