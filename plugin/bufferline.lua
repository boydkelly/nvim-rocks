-- "akinsho/nvim-bufferline.lua",
local bufferline_opts = {
  options = {
    -- numbers = "buffer_id",
    mode = "buffers", -- tabs or buffers
    always_show_bufferline = true,
    close_command = "bdelete! %d",
    right_mouse_command = nil,
    left_mouse_command = "buffer %d",
    middle_mouse_command = nil,

    indicator = {
      icon = "▎", -- this should be omitted if indicator style is not 'icon'
      style = "icon",
    },
    -- diagnostics = "nvim_lsp",
    -- problem with icons.diagnostics (a nil value)
    diagnostics = false,
    diagnostics_indicator = function(_, _, diag)
      local icons = require("config").icons.diagnostics
      local ret = (diag.error and icons.Error .. diag.error .. " " or "")
          .. (diag.warning and icons.Warning .. diag.warning or "")
      return vim.trim(ret)
    end,
    offsets = {
      {
        filetype = "NvimTree",
        text = "File Explorer",
        highlight = "Directory",
        text_align = "left",
        separator = true,
      },
    },

    -- separator_style = "thin",
    -- separator_style = "bar",
    tab_size = 12,
    style_preset = 2,     -- minimal
    buffer_close_icon = "󰖭",
    close_icon = "󰅙 ",
    modified_icon = "󰛿",
    left_trunc_marker = "󰄽",
    right_trunc_marker = "󰄾",

    -- numbers = "buffer_id",
    -- always_show_bufferline = false,
    -- -- -- separator_style = "slant" or "padded_slant",
    -- -- show_tab_indicators = true,
    -- show_buffer_close_icons = false,
    -- show_close_icon = false,
    color_icons = true,
    -- enforce_regular_tabs = false,
    custom_filter = function(buf_number, _)
      local tab_num = 0
      for _ in pairs(vim.api.nvim_list_tabpages()) do
        tab_num = tab_num + 1
      end

      if tab_num > 1 then
        if not not vim.api.nvim_buf_get_name(buf_number):find(vim.fn.getcwd(), 0, true) then
          return true
        end
      else
        return true
      end
    end,
  },
}

local module_name = "bufferline"
local status_ok, module = pcall(require, module_name)
if not status_ok then
  vim.notify("Couldn't load module '" .. module_name .. "'")
  do return end
end
-- print(module_name, module)
require(module_name).setup(bufferline_opts)
-- require("bufferline").setup(bufferline_opts)

-- local options = bufferline_opts.options
-- if vim.opt.mousemoveevent then
--   options.hover = {
--     enabled = true,
--     delay = 100,
--     reveal = { "close" },
--   }
-- end

local scope_opts = {
  excluded_filetypes = { "alpha", "dashboard", "neo-tree", "noice", "prompt", "TelescopePrompt" },
}
require("scope").setup(scope_opts)
