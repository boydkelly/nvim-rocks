local status = pcall(require, 'lazy')
local ensure_installed
if not status then
    ensure_installed = false
else
    ensure_installed = require("config").treesitter
end
local opts = {
  auto_install = false, -- not appropriate for rocks
  sync_install = true, -- only applies to ensure_installed
  ensure_installed = ensure_installed,
  highlight = {
    enable = true,
    additional_vim_regex_highlighting = { "org", "markdown" },
    disable = {
      -- "yaml",
      -- "csv",
      -- "tsv"
    },
  },
  indent = {
    enable = true,
    -- disable = "yaml"
  },
  endwise = {
    enable = true,
  },
  matchup = {
    enable = true,
  },
}
require("nvim-treesitter.configs").setup(opts)
