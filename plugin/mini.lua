require("mini.surround").setup()
require("mini.animate").setup()
require("mini.bracketed").setup()
local opts = {
  symbol = "│",
  options = { try_as_border = true },
}
vim.api.nvim_create_autocmd("FileType", {
  pattern = { "help", "alpha", "dashboard", "NvimTree", "NeoTree", "Trouble", "lazy", "mason" },
  callback = function()
    vim.b.miniindentscope_disable = true
  end,
})
require("mini.indentscope").setup(opts)
