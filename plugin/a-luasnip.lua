-- build = "make install_jsregexp"
local ls = require "luasnip"
local types = require "luasnip.util.types"
local opts = {
    history = true,
    delete_check_events = "TextChanged",

    -- Display a cursor-like placeholder in unvisited nodes of the snippet.
    ext_opts = {
      [types.insertNode] = {
        unvisited = {
          virt_text = { { "|", "Conceal" } },
          -- virt_text_pos = "inline",
        },
      },
      [types.exitNode] = {
        unvisited = {
          virt_text = { { "|", "Conceal" } },
          -- virt_text_pos = "inline",
        },
      },
  },
}
-- stylua: ignore
vim.keymap.set("i", "<C-j>", function()
  return require("luasnip").jumpable(1) and "<Plug>luasnip-jump-next" or "<C-j>" end,
  { expr = true, remap = true, silent = true })

vim.keymap.set("s", "<C-j>", function() require("luasnip").jump(1) end)
vim.keymap.set({"i", "s"}, "<C-k>", function() require("luasnip").jump(-1) end)

require("luasnip").setup(opts)
local vs = require "luasnip.loaders.from_vscode"
local lu = require "luasnip.loaders.from_lua"

lu.lazy_load { paths = vim.fn.stdpath "config" .. "/snippets/lua" }
vs.lazy_load { paths = vim.fn.stdpath "config" .. "/snippets/vs" }
vs.lazy_load()

vim.api.nvim_create_user_command("LuaSnipEdit", function()
  require("luasnip.loaders").edit_snippet_files()
end, {})
