local opts = {
  provider_selector = function(bufnr, filetype, buftype)
    local ft_map = {
      yuck = "treesitter",
    }
    local ft_ignore = { "neo-tree", "git", "alpha" }

    for _, ft in ipairs(ft_ignore) do
      ft_map[ft] = ""
    end

    local function fallback(err, provider)
      if type(err) == "string" and err:match "UfoFallbackException" then
        return require("ufo").getFolds(bufnr, provider)
      else
        return require("promise").reject(err)
      end
    end

    return ft_map[filetype]
      or function()
        return require("ufo")
          .getFolds(bufnr, "lsp")
          :catch(function(err)
            return fallback(err, "treesitter")
          end)
          :catch(function(err)
          if buftype == "nofile" then
            return ""
          end
          return fallback(err, "indent")
        end)
      end
  end,
  open_fold_hl_timeout = 400,
  -- close_fold_kinds = { "imports", "comment" },
  preview = {
    win_config = {
      border = { "", "─", "", "", "", "─", "", "" },
      -- winhighlight = "Normal:Folded",
      winblend = 0,
    },
    mappings = {
      scrollU = "<C-u>",
      scrollD = "<C-d>",
      jumpTop = "[",
      jumpBot = "]",
    },
  },

  fold_virt_text_handler = function(virtual_text, foldstart, foldend, width, truncate)
    local max_width = 120 -- FIXME: hardcode this for now
    local result = {}

    local total_lines = vim.api.nvim_buf_line_count(0)
    local folded_lines = foldend - foldstart

    local fold_hint = ("  %d %d%%"):format(folded_lines, folded_lines / total_lines * 100)
    local hint_width = vim.fn.strdisplaywidth(fold_hint)
    local target_width = width - hint_width
    local cur_width = 0

    for _, chunk in ipairs(virtual_text) do
      local chunk_text = chunk[1]
      local chunk_width = vim.fn.strdisplaywidth(chunk_text)

      if target_width > cur_width + chunk_width then
        table.insert(result, chunk)
      else
        chunk_text = truncate(chunk_text, target_width - cur_width)

        local hl = chunk[2]
        table.insert(result, { chunk_text, hl })

        chunk_width = vim.fn.strdisplaywidth(chunk_text)
        -- str width returned from truncate() may less than 2nd argument, need padding
        if cur_width + chunk_width < target_width then
          fold_hint = fold_hint .. (" "):rep(target_width - cur_width - chunk_width)
        end
        break
      end
      cur_width = cur_width + chunk_width
    end

    local padding = math.max(math.min(max_width, width - 1) - cur_width - hint_width, 0)
    fold_hint = (" "):rep(padding) .. fold_hint
    table.insert(result, { fold_hint, "MoreMsg" })
    return result
  end,
}
-- need to add this manually??
local opt = vim.opt
opt.fillchars = {
  eob = " ",
  fold = " ",
  foldopen = "",
  foldsep = " ",
  foldclose = "",
}
opt.foldcolumn = "1"
opt.foldlevel = 99 -- Using ufo provider need a large value, feel free to decrease the value
opt.foldlevelstart = 99 -- do not autofold
opt.foldenable = true

vim.keymap.set("n", "Z",
  function()
    require("ufo").peekFoldedLinesUnderCursor()
  end,
  {  desc = "Hover" })
vim.keymap.set("n",
  "zR",
  function()
    require("ufo").openAllFolds()
  end,
  { desc = "Open all folds" })

vim.keymap.set("n",
  "zM",
  function()
    require("ufo").closeAllFolds()
  end,
  { desc = "Close all folds",
  })
vim.keymap.set("n",
  "zr",
  function()
    require("ufo").openFoldsExceptKinds()
  end,
  { desc = "Fold less"
  })
vim.keymap.set("n",
  "zm",
  function(level)
    require("ufo").closeFoldsWith(level)
  end,
  { desc = "Fold more",
  })
require("ufo").setup(opts)
