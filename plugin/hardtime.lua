local module_name = "hardtime"
local status_ok, module = pcall(require, module_name)
if not status_ok then
  vim.notify("Couldn't load module '" .. module_name .. "'")
  do return end
end
return module.setup()
-- require("hardtime").setup()
