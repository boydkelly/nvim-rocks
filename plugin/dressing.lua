local opts = {
  input = {
    win_options = {
      winhighlight = "NormalFloat:DiagnosticError",
    },
  },
  select = {
    get_config = function(opts)
      if opts.kind == "legendary.nvim" then
        return {
          backend = "telescope",
          telescope = {
            prompt_title = false,
            sorter = require("telescope.sorters").fuzzy_with_index_bias {},
            layout_strategy = "horizontal",
            layout_config = {
              preview_cutoff = false,
              width = function(_, max_columns, _)
                return math.min(max_columns, 80)
              end,
              height = function(_, _, max_lines)
                return math.min(max_lines, 20)
              end,
            },
          },
        }
      else
        return {
          backend = "nui",
          nui = {},
        }
      end
    end,
  },
}
require ("dressing").setup(opts)
