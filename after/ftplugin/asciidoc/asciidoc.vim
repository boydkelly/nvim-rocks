"Function to make table around text in visual line mode; columns separated by 4 or more spaces
"https://vi.stackexchange.com/questions/17606/vmap-and-visual-block-how-do-i-write-a-function-to-operate-once-for-the-entire
function! s:AsciidocTable() range
    execute "'<,'>s/   */|/g"
    execute "'<,'>s/^/|/g"
    execute "'<,'>s/^||/|/g"
    call append(line("'<")-1, '[width="100%",cols="2",frame="topbot",options="header",stripes="even"]')
    call append(line("'<")-1, '|===')
    call append(line("'>"), '|===')
    "execute a:firstline . "," . a:lastline . "s/   */|/g"
    "execute "'<,'>s/(^)[^|]/\1/g"
endfunction

vnoremap <localleader>t :call <sid>AsciidocTable()<CR>

setl comments=://,b:#,:%,:XCOMM,b:-,b:*,b:.,:\|
