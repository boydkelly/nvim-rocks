local opts = { noremap = true, silent = true, buffer = true }

vim.opt.spell = true
vim.wo.foldmethod = "syntax"
vim.wo.foldenable = false

--   "set background=light
--
--   "search in folds doesnt work but defaults include search
-- vim.bo.fdo:append('search')

--  "allow cut virtual block mode colonm
vim.wo.virtualedit = "block"

--  "make an indent to show wrap
--  "prevent vim from inserting line breaks in newly entered text
--  "disable a bunch of formatoptions
vim.bo.textwidth = 0
vim.bo.wrapmargin = 2 -- start wrapping at 2 characters from the right screen end.
vim.wo.wrap = true --soft wrap lines
vim.wo.linebreak = true --break only on word boundries
vim.o.whichwrap = "b,s,<,>,[,]" -- "cursor wrap at line begining and end
vim.wo.showbreak = " »" --indicates a soft wrap break
vim.wo.breakindent = true --makes the soft wrap break indented
vim.wo.breakindentopt = "min:20,shift:2,sbr"
--  "call pencil#init()

--  "lists completion in asciidoc via comments feature
--  fix vim.bo.comments = '://,b:#,:%,:XCOMM,b:-,b:*,b:.,:\|'

--  " Show tabs and trailing characters.
--  "set listchars=tab:»·,trail:·,eol:¬
--  "yeah but do we actually need to format lists in asciidoc with vim?
vim.wo.listchars = "tab:> ,trail:-,nbsp:+"
-- vim.wo.listchars = "eol:* ,tab:> ,trail:· ,nbsp:+"
vim.o.list = true

vim.bo.formatoptions = "tcqr"
-- " tc text, comments; (almost) no effect while textwidth=0, but will format
-- " with gq formatexp
-- " q enables gq to manually format (with formatexp)
-- " r automatically insert comment after <Enter> (exactly what we need for
-- " lists and tables)

-- " NOT USED BELOW
-- " o not good puts comment on insert
-- " n not needed formats numbered list.  we let asciidc do the final
-- " formatting
-- " j remove comment leader when joining lines.  this is a default with a
-- " wierd effect for us as you can't compete a list properly
-- "
vim.bo.smartindent = true -- " copies indent from previous line

vim.opt.formatlistpat = "\\s*" -- Optional leading whitespace
vim.opt.formatlistpat:append "[" -- Start character class
vim.opt.formatlistpat:append "\\[({]\\?" -- |  Optionally match opening punctuation
vim.opt.formatlistpat:append "\\(" -- |  Start group
vim.opt.formatlistpat:append "[0-9]\\+" -- |  |  Numbers
vim.opt.formatlistpat:append "\\|" -- or
vim.opt.formatlistpat:append "[a-zA-Z]\\+" -- |  |  Letters
vim.opt.formatlistpat:append "\\)" -- |  End groupop
vim.opt.formatlistpat:append "[\\]:.)}" -- |  Closing punctuation
vim.opt.formatlistpat:append "]" -- End character class
vim.opt.formatlistpat:append "\\s\\+" -- One or more spaces
vim.opt.formatlistpat:append "\\|" -- or
vim.opt.formatlistpat:append "^\\s*[-–+o*•]\\s\\+" -- Bullet points

vim.cmd [[call Ventilated()]] -- OneSentencePerLine
vim.cmd "syntax match normal /^= / conceal"
vim.keymap.set("v", "t", ":lua require('utils.functions').asciidoctable()<cr>", opts)

-- vim-asciidoctor settings
local wk = require "which-key"
wk.register {
  ["<leader>"] = {
    n = {
      a = {
        name = "+AsciiDoc",
        o = {
          name = "+Open",
        },
        c = {
          name = "+Convert",
        },
      },
    },
  },
}
vim.keymap.set("n", "<leader>naor", "<cmd>AsciidocOpenRAW<cr>", { desc = "Open raw" })
vim.keymap.set("n", "<leader>naop", "<cmd>AsciidocOpenPDF<cr>", { desc = "Open pdf" })
vim.keymap.set("n", "<leader>naoh", "<cmd>AsciidocOpenHTML<cr>", { desc = "Open html" })
vim.keymap.set("n", "<leader>naod", "<cmd>AsciidocOpenDOCX<cr>", { desc = "Open docx" })
-- vim.keymap.set('n', '<leader>aoc', ':silent exec "!/usr/bin/google-chrome " . '"' . expand('%:p') . '"' .  ' &'', opts)

vim.keymap.set("n", "<leader>nach", "<cmd>Asciidoc2HTML<cr>", { desc = "Convert html" })
vim.keymap.set("n", "<leader>nacp", "<cmd>Asciidoc2PDF<cr>", { desc =  "Convert pdf" })
vim.keymap.set("n", "<leader>nacd", "<cmd>Asciidoc2DOCX<cr>", { desc = "Convert docx" })

vim.keymap.set("n", "<leader>nai", "<cmd>AsciidocPastImage<cr>", { desc = "Past image" })

vim.g.asciidoctor_executable = "asciidoctor"

-- What extensions to use for HTML, default `[]`.
--vim.g.asciidoctor_extensions = ['asciidoctor-diagram', 'asciidoctor-rouge']

-- Path to the custom css
--vim.g.asciidoctor_css_path = '~/docs/AsciiDocThemes'

-- Custom css name to use instead of built-in
--vim.g.asciidoctor_css = 'haba-asciidoctor.css' asciidoctor-pdf

-- What to use for PDF, default `asciidoctor-pdf`.
vim.g.asciidoctor_pdf_executable = "asciidoctor-pdf"

-- What extensions to use for PDF, default `[]`.
--vim.g.asciidoctor_pdf_extensions = ['asciidoctor-diagram']

-- Path to PDF themes, default `''`.
vim.g.asciidoctor_pdf_themes_path = "~/_resources/themes"
vim.g.asciidoctor_pdf_fonts_path = "~/_resources/fonts"

vim.g.asciidoctor_pandoc_executable = "pandoc"
--vim.g.asciidoctor_pandoc_data_dir = '~/docs/.pandoc'
--vim.g.asciidoctor_pandoc_other_params = '--toc'
vim.g.asciidoctor_pandoc_reference_doc = "reference.docx"

-- Fold sections, default `0`.
vim.g.asciidoctor_folding = 1
vim.g.asciidoctor_fold_options = 0
-- Conceal *bold*, _italic_, `code` and urls in lists and paragraphs, default `0`.
-- See limitations in end of the README
vim.g.asciidoctor_syntax_conceal = 1

-- Highlight indented text, default `1`.
vim.g.asciidoctor_syntax_indented = 1
--syntax highlighting for languages in [source] blocks
-- List of filetypes to highlight, default `[]`
-- below causing error
-- vim.g.asciidoctor_fenced_languages = '['css', 'html', 'bash', 'vim']'
vim.g.asciidoctor_fenced_languages = { "css", "html", "bash", "vim" }


