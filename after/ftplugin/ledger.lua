local api = vim.api
local map = vim.keymap.set
local opts = { noremap = true, silent = true }

map("i", "<M-8>", ':call ledger#transaction_state_toggle(line("."), " *?!")<CR>', opts)
vim.wo.foldmethod = "syntax"
vim.wo.foldenable = true
vim.g.ledger_extra_options = ""
vim.g.ledger_maxwidth = 80
vim.g.ledger_fold_blanks = 2
vim.g.ledger_fillstring = "........"
vim.g.ledger_bin = "hledger"
vim.g.ledger_is_hledger = true

-- fix me   set file type and put git command to Buffpostwrite
api.nvim_create_autocmd({ "BufWritePost" }, {
	pattern = { "*.journal", "*.ledger" },
	command =
	':silent! if git rev-parse --git-dir > /dev/null 2>&1 ; then git add % ; git commit -m "Auto-commit: saved %"; fi > /dev/null 2>&1',
})
