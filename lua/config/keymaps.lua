local opts = { noremap = true, silent = true }
local utils = require "utils"

-- Remap for dealing with word wrap
vim.keymap.set("n", "k", "v:count == 0 ? 'gk' : 'k'", { expr = true })
vim.keymap.set("n", "j", "v:count == 0 ? 'gj' : 'j'", { expr = true })

-- Better viewing
-- keymap("n", "n", "nzzzv")
-- keymap("n", "N", "Nzzzv")
-- keymap("n", "g,", "g,zvzz")
-- keymap("n", "g;", "g;zvzz")

-- Scrolling
-- keymap("n", "<C-d>", "<C-d>zz")
-- keymap("n", "<C-u>", "<C-u>zz")
-- keymap("n", "<C-k>", "<C-u>", {opts} )
-- keymap("n", "<C-j>", "<C-d>")
vim.keymap.set("n", "<C-j>", "<PageDown>", opts)
vim.keymap.set("n", "<C-k>", "<PageUp>", opts)

-- Paste
-- keymap("n", "]p", "o<Esc>p", { desc = "Paste below" })
-- keymap("n", "]P", "O<Esc>p", { desc = "Paste above" })

-- Better escape using jk in insert and terminal mode
vim.keymap.set("i", "jk", "<ESC>")
vim.keymap.set("t", "jk", "<C-\\><C-n>")
vim.keymap.set("t", "<C-h>", "<C-\\><C-n><C-w>h")
vim.keymap.set("t", "<C-j>", "<C-\\><C-n><C-w>j")
vim.keymap.set("t", "<C-k>", "<C-\\><C-n><C-w>k")
vim.keymap.set("t", "<C-l>", "<C-\\><C-n><C-w>l")

-- Better indent
vim.keymap.set("v", "<", "<gv")
vim.keymap.set("v", ">", ">gv")

-- Move Lines
vim.keymap.set("n", "<A-j>", "<cmd>m .+1<cr>==", { desc = "Move down" })
vim.keymap.set("n", "<A-k>", "<cmd>m .-2<cr>==", { desc = "Move up" })
vim.keymap.set("i", "<A-j>", "<esc><cmd>m .+1<cr>==gi", { desc = "Move down" })
vim.keymap.set("i", "<A-k>", "<esc><cmd>m .-2<cr>==gi", { desc = "Move up" })
-- from primeagen Neovim Config Part 3 8:04
vim.keymap.set("v", "<A-j>", ":m '>+1<cr>gv=gv", { desc = "Move down" })
vim.keymap.set("v", "<A-k>", ":m '<-2<cr>gv=gv", { desc = "Move up" })

-- Resize window using <shift> arrow keys
vim.keymap.set("n", "<S-Up>", "<cmd>resize +2<CR>")
vim.keymap.set("n", "<S-Down>", "<cmd>resize -2<CR>")
vim.keymap.set("n", "<S-Left>", "<cmd>vertical resize -2<CR>")
vim.keymap.set("n", "<S-Right>", "<cmd>vertical resize +2<CR>")

-- Auto indent
vim.keymap.set("n", "i", function()
  if #vim.fn.getline "." == 0 then
    return [["_cc]]
  else
    return "i"
  end
end, { expr = true })

-- local configs forward

-- Tab switch buffer
-- This inerferes with tab functionality in snippets (try allow remap = true)
-- keymap("n", "<TAB>", ":bnext<CR>", opts)
-- keymap("n", "<S-TAB>", ":bprevious<CR>", opts)

-- File menu

-- wildmenu
-- keymap("c", "j", 'pumvisible() ? "\\<C-n>" : "j"', opts)
-- keymap("c", "k", 'pumvisible() ? "\\<C-p>" : "k"', opts)

-- ctrl to move to other window
vim.keymap.set("n", "<C-l>", "<C-w>w")
vim.keymap.set("n", "<C-h>", "<C-w>w")

--  general

-- map('i', '<Tab>', 'pumvisible() ? "\\<C-n>" : "\\<Tab>"', {noremap = true})
vim.keymap.set("t", "<Esc>", "<C-\\><C-n>", opts)
vim.keymap.set("i", "<A-i>", "<Esc>", opts)
vim.keymap.set("n", "<^s>", "[s", opts)
vim.keymap.set("n", "<s>", "]s", opts)

-- cancel search highlighting with ESC
vim.keymap.set("n", "<ESC>", ":nohlsearch<Bar>:echo<CR>", opts)

-- Toggle french guillemets
vim.keymap.set("n", "<F10>", ":lua require('utils').toggleGuillemets()<cr>", opts)
vim.keymap.set("n", "<F11>", ":lua require('utils').mapGuillemets()<cr>", opts)

-- if utils.has "bufferline.nvim" then
-- keymap("n", "<S-h>", "<cmd>BufferLineCyclePrev<cr>", { desc = "Prev buffer" })
-- keymap("n", "<S-l>", "<cmd>BufferLineCycleNext<cr>", { desc = "Next buffer" })

vim.keymap.set("n", "<S-h>", "<cmd>bprevious<cr>", { desc = "Prev buffer" })
vim.keymap.set("n", "<leader>bp", "<cmd>bprevious<cr>", { desc = "Prev buffer" })
vim.keymap.set("n", "<S-l>", "<cmd>bnext<cr>", { desc = "Next buffer" })
vim.keymap.set("n", "<leader>bn", "<cmd>bnext<cr>", { desc = "Next buffer" })
-- end

-- Clear search, diff update and redraw
-- taken from runtime/lua/_editor.lua
vim.keymap.set(
"n",
"<leader>uR",
"<Cmd>nohlsearch<Bar>diffupdate<Bar>normal! <C-L><CR>",
{ desc = "Redraw / clear hlsearch / diff update" }
)

-- https://github.com/mhinz/vim-galore#saner-behavior-of-n-and-n
vim.keymap.set("n", "n", "'Nn'[v:searchforward]", { expr = true, desc = "Next search result" })
vim.keymap.set("x", "n", "'Nn'[v:searchforward]", { expr = true, desc = "Next search result" })
vim.keymap.set("o", "n", "'Nn'[v:searchforward]", { expr = true, desc = "Next search result" })
vim.keymap.set("n", "N", "'nN'[v:searchforward]", { expr = true, desc = "Prev search result" })
vim.keymap.set("x", "N", "'nN'[v:searchforward]", { expr = true, desc = "Prev search result" })
vim.keymap.set("o", "N", "'nN'[v:searchforward]", { expr = true, desc = "Prev search result" })

-- Add undo break-points
vim.keymap.set("i", ",", ",<c-g>u")
vim.keymap.set("i", ".", ".<c-g>u")
vim.keymap.set("i", ";", ";<c-g>u")

-- save file
vim.keymap.set({ "i", "x", "n", "s" }, "<C-s>", "<cmd>w<cr><esc>", { desc = "Save file" })

--keywordprg
vim.keymap.set("n", "<leader>K", "<cmd>norm! K<cr>", { desc = "Keywordprg" })

-- new file
vim.keymap.set("n", "<leader>fn", "<cmd>enew<cr>", { desc = "New File" })

vim.keymap.set("n", "<leader>xl", "<cmd>lopen<cr>", { desc = "Location List" })
vim.keymap.set("n", "<leader>xq", "<cmd>copen<cr>", { desc = "Quickfix List" })

-- formatting
vim.keymap.set({ "n", "v" }, "<leader>cf", "<cmd>Format<cr>", { desc = "Format" })
vim.keymap.set({ "n", "v" }, "<leader>ctf", "<cmd>FormatDisable<cr>", { desc = "Disable Format" })
vim.keymap.set({ "n", "v" }, "<leader>ctF", "<cmd>FormatEnable<cr>", { desc = "Enable Format" })
-- keymap("n", "<leader>cf", function() utils.format.toggle() end, { desc = "Toggle auto format (global)" })
-- keymap("n", "<leader>CF", function() utils.format.toggle(true) end, { desc = "Toggle auto format (buffer)" })

-- toggle options
vim.keymap.set("n", "<leader>uns", function() utils.toggle("spell") end, { desc = "Toggle Spelling" })
vim.keymap.set("n", "<leader>unw", function() utils.toggle("wrap") end, { desc = "Toggle Word Wrap" })
vim.keymap.set("n", "<leader>unL", function() utils.toggle("relativenumber") end, { desc = "Toggle Relative Line Numbers" })
vim.keymap.set("n", "<leader>unl", function() utils.toggle.number() end, { desc = "Toggle Line Numbers" })
vim.keymap.set("n", "<leader>ctd", function() utils.toggle.diagnostics() end, { desc = "Toggle Diagnostics" })
local conceallevel = vim.o.conceallevel > 0 and vim.o.conceallevel or 3
vim.keymap.set("n", "<leader>uc", function() utils.toggle("conceallevel", false, {0, conceallevel}) end, { desc = "Toggle Conceal" })

-- quit
vim.keymap.set("n", "<leader>qq", "<cmd>qa<cr>", { desc = "Quit all" })

-- windows
vim.keymap.set("n", "<leader>-", "<C-W>s", { desc = "Split window below", remap = true })
vim.keymap.set("n", "<leader>|", "<C-W>v", { desc = "Split window right", remap = true })

-- tabs
vim.keymap.set("n", "<leader><tab>l", "<cmd>tablast<cr>", { desc = "Last Tab" })
vim.keymap.set("n", "<leader><tab>f", "<cmd>tabfirst<cr>", { desc = "First Tab" })
vim.keymap.set("n", "<leader><tab><tab>", "<cmd>tabnew<cr>", { desc = "New Tab" })
vim.keymap.set("n", "<leader><tab>]", "<cmd>tabnext<cr>", { desc = "Next Tab" })
vim.keymap.set("n", "<leader><tab>d", "<cmd>tabclose<cr>", { desc = "Close Tab" })
vim.keymap.set("n", "<leader><tab>[", "<cmd>tabprevious<cr>", { desc = "Previous Tab" })

-- legendary
vim.keymap.set("n", "<C-A-P>", ":Legendary<cr>", opts)
vim.keymap.set("n", "<leader>P", ":Legendary<cr>", opts)
vim.keymap.set("n","<leader>uK", "<cmd>Telescope keymaps<cr>", {desc = "Keymaps" })

-- Code
vim.keymap.set("n","<leader>cl", "<cmd>LspInfo<cr>", {desc = "Lsp Info" })
vim.keymap.set("n","<leader>cm", "<cmd>Mason<cr>", {desc = "Mason" })
vim.keymap.set("n","<leader>cF", "<cmd>ConformInfo<cr>",{desc = "Format Info" })
-- new feature must be enabled in lspconfig.  does lspzero support this?
vim.keymap.set('n', '<leader>ch', function() vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled()) end, {desc = "Inlay hint toggle" })
-- trouble
vim.keymap.set("n", "<leader>cd", "<cmd>TroubleToggle document_diagnostics<cr>", { desc = "Document Diagnostics" })
vim.keymap.set("n", "<leader>cD", "<cmd>TroubleToggle workspace_diagnostics<cr>", { desc = "Workspace Diagnostics" })

-- Terminal Mappings
vim.keymap.set("t", "<esc><esc>", "<c-\\><c-n>", { desc = "Enter Normal Mode" })
vim.keymap.set("t", "<C-h>", "<cmd>wincmd h<cr>", { desc = "Go to left window" })
vim.keymap.set("t", "<C-j>", "<cmd>wincmd j<cr>", { desc = "Go to lower window" })
vim.keymap.set("t", "<C-k>", "<cmd>wincmd k<cr>", { desc = "Go to upper window" })
vim.keymap.set("t", "<C-l>", "<cmd>wincmd l<cr>", { desc = "Go to right window" })
vim.keymap.set("t", "<C-/>", "<cmd>close<cr>", { desc = "Hide Terminal" })
vim.keymap.set("t", "<c-_>", "<cmd>close<cr>", { desc = "which_key_ignore" })
vim.keymap.set("n", "<leader>1", "<Cmd>ToggleTerm<Cr>", { desc = "Term #1" })
vim.keymap.set("n", "<leader>2", "<Cmd>2ToggleTerm<Cr>", { desc = "Term #2" })

-- nvim-tree
vim.keymap.set("n", "<A-e>", ":NvimTreeToggle<CR>", key_opts)
vim.keymap.set("n", "<leader>fe", "<cmd>NvimTreeToggle<cr>", {desc = "Nvim Tree" })

-- File
vim.keymap.set("n", "<leader>fl", ":lua require('luasnip.loaders').edit_snippet_files()<cr>", {desc = "Edit snippets" })
vim.keymap.set("n", "<leader>fw", "<cmd>update!<CR>", {desc = "Save" })
vim.keymap.set("n", "<leader>fW>", "<cmd>noautocmd w<cr>", {desc = "Save without formatting (noautocmd)" })

-- git
vim.keymap.set("n", "<leader>g<cr>", "<cmd>Neogit<cr>", { desc = "Neogit" })

-- Session
vim.keymap.set("n", "<leader>Sr", function() require("persistence").load() end, { desc = "Restore session" }) 
vim.keymap.set("n", "<leader>Sl", function() require("persistence").load({ last = true })end, {desc = "Restore last session"})
vim.keymap.set("n", "<leader>Sx", function() require("persistence").stop() end, {desc = "Don't save current session"})
vim.keymap.set("n", "<leader>Sq", function() require("utils").quit()end, {desc = "Quit"})

-- Ui
-- Keyboard
vim.keymap.set("n", "<leader>ukd", "<cmd>set spelllang=dyu<cr>",{ desc = "Julakan" })
vim.keymap.set("n", "<leader>uke", "<cmd>set spelllang=en<cr>", { desc = "English" })
vim.keymap.set("n", "<leader>ukf", "<cmd>set spelllang=fr<cr>", { desc = "Français" })

--zenmode
vim.keymap.set("n", "<leader>ut", "<cmd>Twilight<cr>",{desc = "Toggle Twighlight" })
vim.keymap.set("n", "<leader>uz", "<cmd>ZenMode<cr>", {desc = "Toggle Zenmode" })
vim.keymap.set("n", "<leader>ub", ":lua require('utils').toggleBar()<cr>", {desc = "Bar Toggle" })

-- highlights under cursor
vim.keymap.set("n", "<leader>ui", vim.show_pos, { desc = "Position highlight group" })
vim.keymap.set("n", "<leader>uh", "<cmd>Telescope highlights<cr>", { desc = "Document highlight groups" })


--plugin manager
local status = pcall(require, 'lazy')
if not status then
  vim.keymap.set("n", "<leader>ub>", ":lua require('utils').toggleBar()<cr>", {desc = "Bar Toggle" })
  vim.keymap.set("n", "<leader>ume", "<cmd>Rocks edit<cr>", {desc = "Rocks edit"})
  vim.keymap.set("n", "<leader>ums", "<cmd>Rocks sync<cr>", {desc = "Rocks sync"})
  vim.keymap.set("n", "<leader>umu", "<cmd>Rocks update<cr>", {desc = "Rocks update"})
else
  vim.keymap.set("n", "<leader>um", function()require("lazy").show() end, {desc = "Manage plugins"})
end
