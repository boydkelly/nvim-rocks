local conditions = require "config.statusline.conditions"
local Job = require "plenary.job"
local palette = require "config.colors"
local colors = palette.oxocarbon
local icons = require "config.icons"

local function fg(name)
  return function()
    local hl = vim.api.nvim_get_hl_id_by_name(name)
    return hl and hl.foreground and { fg = string.format("#%06x", hl.foreground) }
  end
end

local function diff_source()
  local gitsigns = vim.g.gitsigns_head
  if gitsigns then
    return {
      added = gitsigns.added,
      modified = gitsigns.changed,
      removed = gitsigns.removed,
    }
  end
end

return {
  mode0 = {
    "mode",
  },
  mode1 = {
    function()
      return vim.fn.mode()
      -- return vim.g.mode
    end,
    padding = { left = 0, right = 0 },
    color = {},
    cond = nil,
  },
  mode2 = {
    function()
      local alias = {
        [""] = "VB",
        c = "COM",
        n = "NOR",
        no = "RO",
        v = "SEL",
        V = "VL",
        s = "CS",
        S = "LS",
        i = "INS",
        R = "REP",
        Rv = "RV",
        t = "",
      }
      return alias[vim.fn.mode()]
    end,
  },
  branch = {
    "b:gitsigns_head",
    icon = icons.git.Branch,
  },
  git_repo = {
    function()
      local results = {}
      local job = Job:new {
        command = "git",
        args = { "rev-parse", "--show-toplevel" },
        cwd = vim.fn.expand "%:p:h",
        on_stdout = function(_, line)
          table.insert(results, line)
        end,
      }
      job:sync()
      if results[1] ~= nil then
        return vim.fn.fnamemodify(results[1], ":t")
      else
        return ""
      end
    end,
  },

  filename = {
    "filename",
    symbols = { modified = icons.ui.Modified },
    file_status = true,
    newfile_status = true,
    path = 0,
    -- show_filename_only = true, -- Shows shortened relative path when set to false.
    hide_filename_extension = false, -- Hide filename extension when set to true.
    show_modified_status = true, -- Shows indicator when the buffer is modified.
    max_length = vim.o.columns * 2 / 3, -- Maximum width of buffers component,
    cond = nil,
  },

  diff = {
    "diff",
    symbols = { added = "+", modified = "● ", removed = "-" },
    padding = { left = 2, right = 1 },
    diff_color = {
      added = { fg = colors.green },
      modified = { fg = colors.magneta },
      removed = { fg = colors.red },
    },
    cond = nil,
  },
  diff2 = {
    "diff",
    sources = { diff_source },
  },
  python_env = {
    function()
      local utils = require "core.plugins.lualine.utils"
      if vim.bo.filetype == "python" then
        local venv = os.getenv "CONDA_DEFAULT_ENV" or os.getenv "VIRTUAL_ENV"
        if venv then
          local icons = require "nvim-web-devicons"
          local py_icon, _ = icons.get_icon ".py"
          return string.format(" " .. py_icon .. " (%s)", utils.env_cleanup(venv))
        end
      end
      return ""
    end,
    color = { fg = colors.green },
    cond = conditions.hide_in_width,
  },
  diagnostics = {
    "diagnostics",
    sources = { "nvim_diagnostic" },
    diagnostics_color = {
      error = "DiagnosticError",
      warn = "DiagnosticWarn",
      info = "DiagnosticInfo",
      hint = "DiagnosticHint",
    },
    symbols = {
      error = icons.diagnostics.BoldError .. " ",
      warn = icons.diagnostics.BoldWarning .. " ",
      info = icons.diagnostics.BoldInformation .. " ",
      hint = icons.diagnostics.BoldHint .. " ",
    },
    cond = conditions.hide_in_width,
  },
  command = {
    function()
      return require("noice").api.status.command.get()
    end,
    cond = function()
      return package.loaded["noice"] and require("noice").api.status.command.has()
    end,
    color = { fg = colors.white },
  },
  -- recording status
  status_mode = {
    function()
      return require("noice").api.status.mode.get()
    end,
    cond = function()
      return package.loaded["noice"] and require("noice").api.status.mode.has()
    end,
    color = { fg = colors.red },
  },

  recording = {
    function()
      local recording_register = vim.fn.reg_recording()
      if recording_register == "" then
        return ""
      else
        return icons.ui.Target .. recording_register
      end
    end,
    color = { fg = colors.yellow },
  },

  treesitter = {
    function()
      local icon = icons.ui.Tree
      return icon
    end,
    color = function()
      local buf = vim.api.nvim_get_current_buf()
      local ts = vim.treesitter.highlighter.active[buf]
      return {
        fg = ts and not vim.tbl_isempty(ts) and colors.green or colors.red,
      }
    end,
    cond = conditions.nofiletype or vim.bo.filetype ~= "oil"
    -- this will make useless the color but for now lets try it
    -- cond = conditions.buffer_not_empty()
  },

  keymap = {
    function()
      if vim.opt.iminsert:get() > 0 and vim.b.keymap_name then
        return "⌨  " .. vim.b.keymap_name
      end
      return ""
    end,
  },
  language = {
    function()
      return require("hydra.statusline").get_hint()
    end,
  },
  lsp_client = {
    function(msg)
      msg = msg or ""
      local buf_clients = vim.lsp.get_clients({ bufnr = 0 })
      if next(buf_clients) == nil then
        if type(msg) == "boolean" or #msg == 0 then
          return ""
        end
        return msg
      end

      local buf_ft = vim.bo.filetype
      local buf_client_names = {}

      -- add client
      for _, client in pairs(buf_clients) do
        if client.name ~= "null-ls" then
          table.insert(buf_client_names, client.name)
        end
      end

      -- -- add formatter
      -- local lsp_utils = require "plugins.lsp.utils"
      -- local formatters = lsp_utils.list_formatters(buf_ft)
      -- vim.list_extend(buf_client_names, formatters)
      --
      -- -- add linter
      -- local linters = lsp_utils.list_linters(buf_ft)
      -- vim.list_extend(buf_client_names, linters)
      --
      -- -- add hover
      -- local hovers = lsp_utils.list_hovers(buf_ft)
      -- vim.list_extend(buf_client_names, hovers)
      --
      -- -- add code action
      -- local code_actions = lsp_utils.list_code_actions(buf_ft)
      -- vim.list_extend(buf_client_names, code_actions)

      local hash = {}
      local client_names = {}
      for _, v in ipairs(buf_client_names) do
        if not hash[v] then
          client_names[#client_names + 1] = v
          hash[v] = true
        end
      end
      table.sort(client_names)
      return icons.ui.Code .. " " .. table.concat(client_names, ", ") .. " " .. icons.ui.Code
    end,
    -- icon = icons.ui.Code,
    colored = true,
    on_click = function()
      vim.cmd [[LspInfo]]
    end,
  },
  schema = {
    function()
      if vim.bo.filetype == "yaml" then
        local buf = vim.api.nvim_get_current_buf()
        -- This caused huge statup slowdown; disabled yaml-companion
        local schema = require("yaml-companion").get_buf_schema(buf)
        -- if schema.result[1].name == "none" then
        --   return "none"
        -- end
        return schema.result[1].name
      end
    end,
  },
  startup = {
    function()
      local stats = require("lazy").stats()
      local ms = (math.floor(stats.startuptime * 100 + 0.5) / 100)
      return "τ " .. ms .. "ms"
    end,
  },
  spaces = {
    function()
      local shiftwidth = vim.api.nvim_get_option_value("shiftwidth", {scope = "local"})
      return icons.ui.Tab .. " " .. shiftwidth
    end,
    padding = 1,
  },
  column = {
    "%c",
  },
  encoding = {
    "o:encoding",
    fmt = string.upper,
    color = {},
    cond = conditions.hide_in_width,
  },
  filetype = { "filetype", cond = nil, padding = { left = 1, right = 1 } },
  hexchar = { "%B" },
  csv = {
    function()
      return require("noice").api.status.message.get()
    end,
    cond = conditions.iscsv,
  },
  location = {
    "location",
    cond = conditions.notcsv,
  },
  progress = {
    "progress",
    fmt = function()
      return "%P/%L"
    end,
    color = {},
    cond = conditions.notcsv,
  },
  scrollbar = {
    function()
      local current_line = vim.fn.line "."
      local total_lines = vim.fn.line "$"
      local chars = {
        "__",
        "▁▁",
        "▂▂",
        "▃▃",
        "▄▄",
        "▅▅",
        "▆▆",
        "▇▇",
        "██",
      }
      local line_ratio = current_line / total_lines
      local index = math.ceil(line_ratio * #chars)
      return chars[index]
    end,
    padding = { left = 0, right = 0 },
    color = { fg = colors.blue, bg = colors.bright.black, gui = "bold" },
    cond = nil,
  },
}
