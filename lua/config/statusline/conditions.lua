local window_width_limit = 100

local conditions = {
  buffer_not_empty = function()
    return vim.fn.empty(vim.fn.expand "%:t") ~= 1
  end,

  nofiletype = function()
    return vim.bo.filetype ~= ""
  end,

  hide_in_width = function()
    return vim.o.columns > window_width_limit
  end,

  check_git_workspace = function()
    local filepath = vim.fn.expand "%:p:h"
    local gitdir = vim.fn.finddir(".git", filepath .. ";")
    return gitdir and #gitdir > 0 and #gitdir < #filepath
  end,

  treesitter = function()
    local buf = vim.api.nvim_get_current_buf()
    local ts = vim.treesitter.highlighter.active[buf]
    return vim.tbl_isempty(ts)
  end,

  notcsv = function()
    return vim.bo.filetype ~= "csv" and vim.bo.filetype ~= "tsv"
  end,

  iscsv = function()
    return vim.bo.filetype == "csv" or vim.bo.filetype == "tsv"
  end,
}

return conditions

