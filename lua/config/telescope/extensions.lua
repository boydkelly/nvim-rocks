local fb_actions = require("telescope").extensions.file_browser.actions
return {
  -- ["ui-select"] = {
  --   require("telescope.themes").get_dropdown {
  --     -- even more opts
  --   },
  -- },
  file_browser = {
    hijack_netrw = true,
    hidden = true,
    mappings = {
      i = {
        ["<c-n>"] = fb_actions.create,
        ["<c-r>"] = fb_actions.rename,
        -- ["<c-h>"] = actions.which_key,
        ["<c-h>"] = fb_actions.toggle_hidden,
        ["<c-x>"] = fb_actions.remove,
        ["<c-p>"] = fb_actions.move,
        ["<c-y>"] = fb_actions.copy,
        ["<c-a>"] = fb_actions.select_all,
      },
    },
  },

  -- zoxide = {
  --   prompt_title = "[Zoxide: CR=cd; C-f=find files; C-q/C-v=split]",
  -- },
  -- project = {
  --   base_dirs = {
  --     { "~/dev", max_depth = 3 },
  --     { "~/.config", max_depth = 2 },
  --   },
  --   hidden_files = true, -- default: false
  --   -- theme = "dropdown",
  --   order_by = "asc",
  --   search_by = "title",
  --   sync_with_nvim_tree = true, -- default false
  --   -- default for on_project_selected = find project files
  --   on_project_selected = function(prompt_bufnr)
  --     -- Do anything you want in here. For example:
  --     project_actions.change_working_directory(prompt_bufnr, true)
  --     require("harpoon.ui").nav_file(1)
  --   end,
  -- },
  repo = {
    list = {
      fd_opts = {
        "--no-ignore-vcs",
      },
      search_dirs = {
        "~/dev",
      },
    },
  },
  menu = {
    default = {
      items = {
        { "Checkhealth", "checkhealth" },
        { "Language", "Telescope menu language" },
        { display = "Bar Toggle", value = ":lua require('utils').toggleBar()" },
        {
          display = "Colorscheme",
          value = ":lua require('telescope.builtin').colorscheme({enable_preview = true})",
        },
        { display = "Run command", value = "Telescope commands" },
        {
          display = "Manage plugins",
          value = function()
            require("utils").toggle_float()
          end,
        },
        { display = "Twighlight", value = "Twilight" },
        { display = "Zenmode", value = "ZenMode" },
      },
    },
    language = {
      items = {
        -- You can add an item of menu in the form of { "<display>", "<command>" }
        { "French", "set spelllang=fr" },
        { "English", "set spelllang=en" },
        { "Dyula", "set spelllang=dyu" },
      },
    },
  },
  fzf = {
    fuzzy = true, -- false will only do exact matching
    override_generic_sorter = true, -- override the generic sorter
    override_file_sorter = true, -- override the file sorter
    case_mode = "smart_case", -- or "ignore_case" or "respect_case" or "smart_case"
  },
}
