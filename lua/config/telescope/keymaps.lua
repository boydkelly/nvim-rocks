local builtin = require("telescope.builtin")
local utils = require("telescope.utils")

vim.keymap.set("n", "<leader>uC", function() require('telescope.builtin').colorscheme() end, { desc = "Colorscheme with preview" })

-- vim.keymap.set("n", "<leader>;", "<cmd>Telescope menu theme=dropdown<cr>", { desc = "Menu", remap = true })
vim.keymap.set("n", "<leader>bb", "<cmd>Telescope buffers previewer=false<cr>", { desc = "Buffers" })
vim.keymap.set("n", "<leader>bm", "<cmd>Telescope marks<cr>", { desc = "Marks" })
vim.keymap.set("n", "<leader>bs", function() builtin.current_buffer_fuzzy_find() end,
{ desc = "Buffer", })
-- vim.keymap.set("n", "<leader>co", "<cmd>Telescope aerial<cr>", { desc = "Code Outline" })
vim.keymap.set("n", "<leader>fb", "<cmd>Telescope buffers sort_mru=true sort_lastused=true<cr>", { desc = "Buffers" })
vim.keymap.set("n", "<leader>fc", function() builtin.find_files({ cwd = vim.env.HOME .. '/.config' }) end,
{ desc = "XDG config files" })
vim.keymap.set("n", "<leader>fC", function() builtin.find_files({ cwd = vim.fn.stdpath('config') }) end, { desc = "Neovim config files" })
vim.keymap.set("n", "<leader>ff", require("telescope.builtin").find_files, { desc = "Find Files (Root Dir?)" })
vim.keymap.set("n", "<leader>fF", function() builtin.find_files({ cwd = utils.buffer_dir() }) end,
{ desc = "Find files in cwd" })
vim.keymap.set("n", "<leader>fi", "<cmd>Telescope file_browser<cr>", { desc = "Browser" })
vim.keymap.set("n", "<leader>fp", function() builtin.find_files({ cwd = vim.fn.stdpath('state')})end,{ desc = "Installed plugins" })
vim.keymap.set("n","<leader>fP", function() builtin.find_files ({ cwd = vim.fn.stdpath('data') })end,{ desc = "Find Plugin File" })
vim.keymap.set("n", "<leader>fr", "<cmd>Telescope oldfiles<CR>", { desc = "Recent Files" })
vim.keymap.set("n", "<leader>fR", function() builtin.oldfiles ({cwd = utils.buffer_dir()}) end, { desc = "Recent Files cwd" })
-- git
-- vim.keymap.set("n", "<leader>gc", "<cmd>Telescope git_commits<CR>", { desc = "commits" })
-- vim.keymap.set("n", "<leader>gs", "<cmd>Telescope git_status<CR>", { desc = "status" })
--   -- {"<leader>gf", require("plugins.telescope.pickers").git_diff_picker, { desc = "Diff Files" })
-- vim.keymap.set("n", "<leader>gb", "<cmd>Telescope git_branches<cr>", { desc = "Checkout branch" })
-- vim.keymap.set("n", "<leader>gm", "<cmd>Telescope git_commits<cr>", { desc = "Checkout commit" })
-- vim.keymap.set("n", "<leader>gM", "<cmd>Telescope git_bcommits<cr>", { desc = "Checkout commit(for current file)" })
-- help
vim.keymap.set("n", "<leader>hs", "<cmd>Telescope help_tags<cr>", { desc = "Help tags" })
vim.keymap.set("n", "<leader>hm", "<cmd>Telescope man_pages<cr>", { desc = "Man pages" })
-- vim.keymap.set("n", "<leader>hp", "<cmd>Telescope lazy<cr>", { desc = "Plugin readme's" })
-- project
-- {
--   "<leader>pp",
--   function()
--     require("telescope").extensions.project.project { display_type = "minimal" }
--   end,
--   { desc = "Project [ctrl + d b a s l r f w o]",
-- })
vim.keymap.set("n", "<leader>pp", "<cmd>Telescope projects theme=dropdown<cr>", { desc = "Projects [ctrl + f b d s r w]" })
-- { "<leader>pP", require("telescope").extensions.projects.projects { { desc = "Projects [ctrl + f b d s r w]" } })
vim.keymap.set("n", "<leader>pr", "<cmd>Telescope repo list<cr>", { desc = "Dynamic projects repo" })
vim.keymap.set("n", "<leader>pc", "<cmd>cd %:p:h<cr>", { desc = "Change WorkDir to current file" })

-- search

-- { "<leader>sb", utils.telescope("current_buffer_fuzzy_find", { cwd = true }), { desc = "Buffer" })
-- { "<leader>sb", require("telescope.builtin").current_buffer_fuzzy_find(), "Buffer" })
--search
--vim.keymap.set("n",
--   "<leader>fg",
--   function()
--     require("telescope").extensions.live_grep_args.live_grep_args()
--   end,
--   { desc = "Live Grep",
-- })
vim.keymap.set("n", "<leader>sc", "<cmd>Telescope commands<cr>", { desc = "Commands" })
vim.keymap.set("n",
  "<leader>sf",
  "<cmd>lua require'telescope.builtin'.grep_string{ shorten_path = true, word_match = '-w', only_sort_text = true, search = '' }<cr>",
  { desc = "Fuzzy search",
  })
vim.keymap.set("n", "<leader>sh", "<cmd>Telescope heading<cr>", { desc = "Headings" })
vim.keymap.set("n", "<leader>sJ", "<cmd>lua require('config.telescope').mande()<cr>", { desc = "Search Jula" })
vim.keymap.set("n", "<leader>sK", "<cmd>lua require('config.telescope').manden_string()<cr>", { desc = "??Search mande keyword" })
vim.keymap.set("n", "<leader>sk", "<cmd>Telescope keymaps<cr>", { desc = "Keymaps" })
vim.keymap.set("n", "<leader>sl", "<cmd>Telescope resume<cr>", { desc = "Resume last search" })
vim.keymap.set("n", "<leader>sm", "<cmd>lua require('config.telescope').mandenkan()<cr>", { desc = "Search Mandenkan" })
vim.keymap.set("n", "<leader>sO", "<cmd>Telescope vim_options<cr>", { desc = "Vim Options" })
vim.keymap.set("n", "<leader>sR", "<cmd>Telescope registers<cr>", { desc = "Registers" })
vim.keymap.set("n", "<leader>ss", "<cmd>Telescope grep_string<cr>", { desc = "Text under cursor" })
vim.keymap.set("n", "<leader>sS", "<cmd>Telescope symbols<cr>", { desc = "Symbols" })
vim.keymap.set("n", "<leader>s:", "<cmd>Telescope search_history<cr>", { desc = "Search History" })
vim.keymap.set("n", "<leader>s;", "<cmd>Telescope command_history<cr>", { desc = "Command history" })
vim.keymap.set("n",
  "<leader>su",
  function()
    require("telescope.builtin").live_grep { search_dirs = { vim.fs.dirname(vim.fn.expand "%") } }
  end,
  { desc = "Grep (Current File Path)",
  })
vim.keymap.set("n", "<leader>sw", function() builtin.live_grep() end, { desc = "Grep root dir)" })
vim.keymap.set("n", "<leader>sw", function() builtin.live_grep({cwd = utils.buffer_dir()}) end, { desc = "Grep cwd" })

vim.keymap.set("n", "<leader>sU", "<cmd>lua require('config.telescope').unicode()<cr>", { desc = "Search Unicode" })
-- Ui
vim.keymap.set("n", "<leader>uc;", "<cmd>Telescope commands<cr>", { desc = "Run Command" })
