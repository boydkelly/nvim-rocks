local M = {}

function M.unicode()
  require("telescope.builtin").live_grep {
    search_dirs = { vim.fn.stdpath "data" .. "/site/unicode/UnicodeData.txt" },
    prompt_title = "Unicode",
    preview_title = "Preview",
    layout_strategy = "vertical",
    path_display = { "hidden" },
  }
end

function M.mandenkan()
  require("utils").SetCALayout()
  require("telescope.builtin").live_grep {
    file_ignore_patterns = {
      ".xhtml",
      ".svg",
      ".idx",
      ".dat",
      ".dic",
      ".all.txt",
      ".delete-sed.txt",
    },
    cwd = "$HOME/Documents/Jula/search",
    path_display = { "hidden" },
    prompt_title = "Mandenkan",
    -- preview_title = false,
    layout_strategy = "vertical",
    layout_config = {
      -- preview_cutoff = 120,
      vertical = {
        height = vim.o.lines,
        width = vim.o.columns,
        prompt_position = "top",
        preview_height = 0.4,
        mirror = true,
      },
    },
  }
end

function M.manden_string()
  require("utils").SetCALayout()
  require("telescope.builtin").grep_string {
    file_ignore_patterns = {
      ".xhtml",
      ".svg",
      ".idx",
      ".dat",
      ".dic",
      ".all.txt",
      ".delete-sed.txt",
    },
    cwd = "$HOME/Documents/Jula/search",
    path_display = { "hidden" },
    prompt_title = "Mandenkan",
    -- preview_title = false,
    layout_strategy = "vertical",
    layout_config = {
      -- preview_cutoff = 120,
      vertical = {
        height = vim.o.lines,
        width = vim.o.columns,
        prompt_position = "top",
        preview_height = 0.4,
        mirror = true,
      },
    },
  }
end

function M.mande()
  require("utils").SetCALayout()
  require("telescope.builtin").live_grep {
    cwd = "$HOME/Documents/Jula/search",
    file_ignore_patterns = {
      ".xhtml",
      ".svg",
      ".idx",
      ".dat",
      ".dic",
      ".all.txt",
      ".delete-sed.txt",
    },
    path_display = { "hidden" },
    prompt_title = false,
    -- preview_title = "Preview",
    dynamic_preview_title = true,
    layout_strategy = "horizontal",
    layout_config = {
      width = vim.o.columns,
      height = vim.o.lines,
      -- width = 0.95,
      -- height = 0.95,
      prompt_position = "top",
      preview_width = 0.7,
    },
  }
end

function M.notes()
  require("telescope.builtin").live_grep {
    prompt_title = "Notes",
    path_display = { "smart" },
    cwd = "~/gtd/",
    -- layout_strategy = "horizontal",
    -- layout_config = { preview_width = 0.75, width = 0.65 },
  }
end

function M.startup()
  require("telescope").extensions.menu.startup {
    layout_config = { width = 0.5, height = 15 },
  }
end

M.file_ignore_patterns = {
  "%.7z",
  "%.JPEG",
  "%.JPG",
  "%.MOV",
  "%.RAF",
  "%.burp",
  "%.bz2",
  "%.cache",
  "%.class",
  "%.dll",
  "%.db",
  "%.docx",
  "%.dylib",
  "%.epub",
  "%.exe",
  "%.flac",
  "%.ico",
  "%.ipynb",
  "%.jar",
  "%.jpeg",
  "%.jpg",
  "%.lock",
  "%.mkv",
  "%.mov",
  "%.mp4",
  "%.otf",
  "%.pdb",
  "%.pdf",
  "%.png",
  "%.rar",
  "%.sqlite3",
  "%.svg",
  "%.so",
  "%.tar",
  "%.tar.gz",
  "%.ttf",
  "%.webp",
  "%.zip",
  ".git/",
  ".gradle/",
  ".idea/",
  ".settings/",
  ".vale/",
  ".vscode/",
  "__pycache__/*",
  "build/",
  "env/",
  "gradle/",
  "sessions/",
  "node_modules/",
  "smalljre_*/*",
  "target/",
  "vendor/*",
}

return M
