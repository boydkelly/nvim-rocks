local indent = 2
local options = {

  -- misc
  clipboard = "unnamedplus", -- Sync with system clipboard
  backspace = { "eol", "start", "indent" },
  encoding = "utf-8",
  matchpairs = { "(:)", "{:}", "[:]", "<:>", "«:»" },
  syntax = "enable",

  -- indentation
  autoindent = true,
  expandtab = true, -- Use spaces instead of tabs
  shiftwidth = indent, -- Size of an indent
  smartindent = true, -- Insert indents automatically
  breakindent = true,
  tabstop = indent,
  softtabstop = indent,
  shiftround = true, -- Round indent to indent

  -- search
  hlsearch = false,
  incsearch = true,
  inccommand = "nosplit", -- preview incremental substitute
  ignorecase = true, -- Ignore case
  smartcase = true, -- Don't ignore case with capitals
  wildignore = [[
.git,.hg,.svn
*.aux,*.out,*.toc
*.o,*.obj,*.exe,*.dll,*.manifest,*.rbc,*.class
*.ai,*.bmp,*.gif,*.ico,*.jpg,*.jpeg,*.png,*.psd,*.webp
*.avi,*.divx,*.mp4,*.webm,*.mov,*.m2ts,*.mkv,*.vob,*.mpg,*.mpeg
*.mp3,*.oga,*.ogg,*.wav,*.flac
*.eot,*.otf,*.ttf,*.woff
*.doc,*.pdf,*.cbr,*.cbz
*.zip,*.tar.gz,*.tar.bz2,*.rar,*.tar.xz,*.kgb
*.swp,.lock,.DS_Store,._*
*/tmp/*,*.so,*.swp,*.zip,**/node_modules/**"
]],
  wildmenu = true,
  wildmode = "longest:full,full", -- Command-line completion mode

  -- backup
  autowrite = true, -- Enable auto write
  backup = false, --folke has this true
  backupdir = vim.fn.stdpath "state" .. "/backup",
  swapfile = false,
  writebackup = false,
  undofile = true,
  undodir = vim.fn.stdpath "data" .. "/undodir",
  undolevels = 10000,
  confirm = false, -- Confirm to save changes before exiting modified buffer

  -- autocomplete
  completeopt = "menu,menuone,noselect",
  shortmess = "aotIWFCc",
  -- shortmess = "aoOtI",

  -- performance
  redrawtime = 1500,
  -- timeoutlen = 200,
  timeoutlen = 300,
  ttimeoutlen = 10,
  updatetime = 200, -- Save swap file and trigger CursorHold
  -- updatetime = 100,

  -- ui
  termguicolors = true, -- True color support
  title = true,
  errorbells = false,
  cmdheight = 2,
  ruler = false,
  laststatus = 0, --remove display on startup; set to 3 after by lualine
  showmode = false, -- Dont show mode since we have a statusline
  mouse = "n", -- will I ever use the mouse in vim?
  cursorline = true,
  pumblend = 10, -- Popup blend
  pumheight = 20, -- Maximum number of entries in a popup
  splitbelow = true, -- Put new windows below current
  splitright = true, -- Put new windows right of current
  winminwidth = 20, -- Minimum window width
  background = "dark",
  scrolloff = 2,
  scrollback = 100000,
  sidescrolloff = 8, -- Columns of context
  splitkeep = "screen", --nvim 9 only

  -- gutter
  foldcolumn = "1",
  foldenable = true,
  foldlevel = 99, -- Using ufo provider need a large value, feel free to decrease the value
  foldlevelstart = 99,
  foldmethod = "syntax",
  number = true,
  relativenumber = true,
  signcolumn = "yes", -- Always show the signcolumn, otherwise it would shift the text each time

  -- text
  conceallevel = 0, -- Hide * markup for bold and italic
  list = true, --show invisible characters
  listchars = [[tab:-,trail:·,lead: ,extends:»,precedes:«,nbsp:×]],
  formatoptions = "jcroqlnt", -- was tcqj
  grepformat = "%f:%l:%c:%m",
  grepprg = "rg --vimgrep --smart-case --", -- use rg instead of grep
  fillchars = [[eob: ,fold: ,foldopen:,foldsep: ,foldclose:]],
  wrap = false, -- Disable/enable line wrap
  joinspaces = false,

  -- vi
  exrc = true,
  sessionoptions = { "buffers", "curdir", "tabpages", "winsize" },

  spellsuggest = "best,20"
}

for k, v in pairs(options) do
  vim.opt[k] = v
end

vim.g.mapleader = " "
vim.g.maplocalleader = ","
vim.g.csv_default_delim = ","
--
-- to dw with hyphens
vim.opt.iskeyword:append "-"
-- dont show perl healthcheck
vim.g.loaded_perl_provider = 0
-- Fix markdown indentation settings
vim.g.markdown_recommended_style = 0 --is this for a plugin?
