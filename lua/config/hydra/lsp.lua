-- LSP
require "hydra" {
  name = "LSP",
  hint = [[
					  			LSP Actions
^
	_a_: code action           _r_: rename
	_p_: preview definition    _P_: go to definition
	_t_: type definition       _K_: documentation
	_o_: outline               _d_: line diagnostics
^
										_<Esc>_
					]],
  config = {
    color = "amaranth",
    invoke_on_body = true,
    hint = {
      position = "middle",
    },
  },
  mode = { "n", "x" },
  body = "<leader>C",
  heads = {
    { "a", "<cmd>Lspsaga code_action<cr>", { exit = true } },
    { "r", "<cmd>Lspsaga rename<cr>", { exit = true } },
    { "p", "<cmd>Lspsaga preview_definition<cr>", { exit = true } },
    { "P", "<cmd>Lspsaga goto_definition<cr>", { exit = true } },
    { "t", "<cmd>Lspsaga goto_type_definition<cr>", { exit = true } },
    { "K", "<cmd>Lspsaga hover_doc<cr>", { exit = true } },
    { "o", "<cmd>Lspsaga outline<cr>", { exit = true } },
    { "d", "<cmd>Lspsaga show_line_diagnostics<cr>", { exit = true } },
    { "<Esc>", nil, { exit = true, nowait = true } },
  },
}
