local Hydra = require "hydra"

local hint = [[
        Quick Menu
^
_a_: Abbreviations
_b_: Bible Names
_f_: Geography Foreign
_g_: Geography Dyula
_p_: Proper Names
_P_: Pin (what is this?)
_r_: Remove it! 
_t_: Typos 
_u_: Unclassified

_j_: Add words 
^
^ ^  _q_/_<Esc>_: Exit Hydra
    ]]
local opts = { exit = true, nowait = true }

Hydra {
  --  local cmd = require("hydra.keymap-util").cmd
  hint = hint,
  config = {
    color = "teal",
    invoke_on_body = true,
    hint = {
      type = "window",
      position = "bottom",
      show_name = true,
      float_opts = {},
    },
  },
  mode = { "v" },
  body = "<leader>m",
  heads = {
    {
      "P",
      function()
        vim.cmd "execute w >> ./remove/geo.txt"
      end,
      { desc = "pin" },
    },
    { "a", ":w >> ./remove/abbrev.txt<cr>|gvd", { desc = "Abbreviations", silent = true } },
    { "b", ":w >> ./remove/bible-names.txt<cr>|gvd", { desc = "Bible Names", silent = true } },
    { "f", ":w >> ./remove/geo-foreign.txt<cr>|gvd", { desc = "Geo Foreign", silent = true } },
    { "g", ":w >> ./remove/geo.txt<cr>|gvd", { desc = "Geo Dyula", silent = true } },
    { "j", ":w >> ./addwords.txt<cr>|gvd", { desc = "Add Words", silent = true } },
    { "p", ":w >> ./remove/proper-names.txt<cr>|gvd", { desc = "Proper Names", silent = true } },
    { "r", ":w >> ./remove/remove.txt<cr>|gvd", { desc = "Remove it!", silent = true } },
    { "t", ":w >> ./remove/typos.txt<cr>|gvd", { desc = "Typos", silent = true } },
    { "u", ":w >> ./remove/unclassified.txt<cr>|gvd", { desc = "Geo", silent = true } },
    { "q", nil, { desc = "quit", opts } },
    { "<Esc>", nil, { desc = "quit", opts } },
  },
}
