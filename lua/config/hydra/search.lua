-- TELESCOPE
require "hydra" {
  name = "Telescope",
  hint = [[
           _o_: recent files   _g_: live grep
           _p_: projects       _/_: search in file
           _r_: resume         _f_: find files
   ▁      
           _h_: vim help       _c_: execute command
           _k_: keymaps        _;_: commands history
           _O_: options        _?_: search history
  ^
  _<Esc>_         _<Enter>_: NvimTree
    ]],
  config = {
    color = "amaranth",
    invoke_on_body = true,
    hint = {
      position = "middle",
    },
  },
  mode = { "n", "x" },
  body = "<leader>T",
  heads = {
    { "o", "<cmd>Telescope oldfiles<cr>", { exit = true } },
    { "p", "<cmd>Telescope projects<cr>", { exit = true } },
    { "r", "<cmd>Telescope resume<cr>", { exit = true } },
    { "h", "<cmd>Telescope help_tags<cr>", { exit = true } },
    { "k", "<cmd>Telescope keymaps<cr>", { exit = true } },
    { "O", "<cmd>Telescope vim_options<cr>", { exit = true } },
    { "g", "<cmd>Telescope live_grep<cr>", { exit = true } },
    { "/", "<cmd>Telescope current_buffer_fuzzy_find<cr>", { exit = true } },
    { "f", "<cmd>Telescope find_files<cr>", { exit = true } },
    { "c", "<cmd>Telescope commands<cr>", { exit = true } },
    { ";", "<cmd>Telescope command_history<cr>", { exit = true } },
    { "?", "<cmd>Telescope search_history<cr>", { exit = true } },
    { "<Enter>", "<cmd>Neotree toggle<cr>", { exit = true } },
    { "<Esc>", nil, { exit = true, nowait = true } },
  },
}
