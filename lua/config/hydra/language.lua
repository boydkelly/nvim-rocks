local Hydra = require "hydra"

local function cmd(command)
  return table.concat { "<Cmd>", command, "<CR>" }
end

local hint = [[
 _e_: English          _d_: Dyula 
 _f_: French
]]

Hydra {
  name = "Keymap",
  hint = hint,
  config = {
    color = "amaranth",
    invoke_on_body = true,
    hint = {
      position = "bottom",
    },
  },
  mode = "n",
  body = "<leader>k",
  heads = {
    { "d", cmd "set spelllang=dyu", { exit = true, mode = "n" } },
    { "e", cmd "set spelllang=en", { exit = true, mode = "n" } },
    { "f", cmd "set spelllang=fr", { exit = true, mode = "n" } },
    { "<Esc>", nil, { exit = true, mode = "n" } },
  },
}
