local colors = {
  oxocarbon = {
    black = "#161616", --  nvim bg
    red = "#ee5396",
    green = "#42be65",
    yellow = "#ffe97b",
    blue = "#78a9ff",
    magneta = "#ff7eb6",
    cyan = "#3ddbd9",
    white = "#dde1e6",
    bright = {
      black = "#393939",
      red = "#ee5396",
      green = "#b5bd68",
      yellow = "#f0c674",
      blue = "#81a2be",
      magenta = "#b294bb",
      cyan = "#8abeb7",
      white = "#ffffff",
    },
    NormalFg = "#33b1ff", -- hightlight Normal fg
    NormalBg = "#161616", -- hightlight Normal bg
    ActiveFg = "#33b1ff", -- hightlight StatusLine fg
    ActiveBg = "#161616", -- hightlight StatusLine bg
    InactiveFg = "#393939", -- hightlight StatusLineNc fg
    InactiveBg = "#33b1ff", -- hightlight StatusLineNc bg
    terminal = {
      black = "#161616", -- terminal_color_0,
      red = "#ee5396", -- terminal_color_1,
      green = "#42be65", -- terminal_color_2,
      yellow = "#ffe97b", -- terminal_color_3,
      blue = "#33b1ff", -- terminal_color_4,
      magenta = "#ff7eb6", -- terminal_color_5,
      cyan = "#3ddbd9", -- terminal_color_6,
      white = "#dde1e6", -- terminal_color_7,
      black_light = "#393939", -- terminal_color_8,
      red_light = "#ee5396", -- terminal_color_9,
      green_light = "#b5bd68", -- terminal_color_10,
      yellow_light = "#f0c674", -- terminal_color_11,
      blue_light = "#81a2b8", -- terminal_color_12,
      magenta_light = "#b294bb", -- terminal_color_13,
      cyan_light = "#8abeb7", -- terminal_color_14,
      white_light = "#ffffff", -- terminal_color_15,
    },
    base16 = {
      base00 = "#161616",
      base01 = "#262626",
      base02 = "#393939",
      base03 = "#525252",
      base04 = "#dde1e6",
      base05 = "#f2f4f8",
      base06 = "#ffffff",
      base07 = "#08bdba",
      base08 = "#3ddbd9",
      base09 = "#78a9ff",
      base0A = "#ee5396",
      base0B = "#33b1ff",
      base0C = "#ff7eb6",
      base0D = "#42be65",
      base0E = "#be95ff",
      base0F = "#82cfff",
    },
  },
}
return colors
