return {
  settings = {
    xml = {
      format = {
        enabled = true,
        splitAttributes = false,
        preservedNewlines = 2,
      },
      validation = { enabled = true },
    },
  },
}
