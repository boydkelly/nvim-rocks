return {
  {
    "nvim-treesitter/nvim-treesitter",
    opts = function(_, opts)
      if type(opts.ensure_installed) == "table" then
        vim.list_extend(opts.ensure_installed, { "xml" })
      end
    end,
  },
  {
    "neovim/nvim-lspconfig",
    opts = {
      servers = {
        lemminx = {
          settings = {
            xml = {
              format = {
                enabled = true,
                splitAttributes = false,
                preservedNewlines = 2,
              },
              validation = { enabled = true },
            },
          },
        },
      },
    },
  },
}
