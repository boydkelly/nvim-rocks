local icons = require "config.icons"
local config = {
  underline = true,
  signs = true,
  update_in_insert = false,
  severity_sort = true,
  float = {
    border = "none",
    focusable = false,
    header = { icons.diagnostics.Debug .. " Diagnostics:", "DiagnosticInfo" },
    scope = "line",
    suffix = "",
    source = false,
  },
  virtual_text = {
    prefix = "●",
    spacing = 4,
    source = false,
    severity = {
      min = vim.diagnostic.severity.HINT,
    },
  },
}

return config
