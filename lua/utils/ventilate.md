He lay flat on the brown, pine-needled floor of the forest, his chin on his
folded arms, and high overhead the wind blew in the tops of the pine trees.
The mountainside sloped gently where he lay; but below it was steep and he
could see the dark of the oiled road winding through the pass. There was a
stream alongside the road and far down the pass he saw a mill beside the stream
and the falling water of the dam, white in the summer sunlight. Another
sentence: He replied "what did you say?" She stated, these are “smartquotes.”
Is this a question? Here are some « French quotes. » History is stained with
blood spilled in the name of "civilization." He said, "I may forget your name,
but I never forget a face." She said, 'Some British periods (full stop) fall
outside single quotes'. He replied, "In American English a reference can push
the punctuation outside the quote as well (with a full stop!). Very interesting!
Check the grammar references for more examples.

He lay flat on the brown, pine-needled floor of the forest, his chin on his folded arms, and high overhead the wind blew in the tops of the pine trees.
The mountainside sloped gently where he lay;
but below it was steep and he could see the dark of the oiled road winding through the pass.
There was a stream alongside the road and far down the pass he saw a mill beside the stream and the falling water of the dam, white in the summer sunlight.
Another sentence:
He replied "what did you say?"
She stated, these are “smartquotes.”
Is this a question?
Here are some « French quotes. »
History is stained with blood spilled in the name of "civilization."
He said, "I may forget your name, but I never forget a face."
She said, 'Some British periods (full stop) fall outside single quotes'.
He replied, "In American English a reference can push the punctuation outside the quote as well (with a full stop!).
Very interesting!
Check the grammar references for more examples.
